# Meeting Scheduler System

System requirements for a fictitious meeting scheduler system

## Detailed system requirements

The following requirements are derived from high-level system requirements

### [requirement id=REQ0011 parent=REQ0004]

The system must have the ability to schedule rooms and equipment.

### [requirement id=REQ0012 parent=REQ0004]

The system should be able to set attributes of a resource (e.g. room capacity, time of day, resource owner, who is authorized, or special features).
