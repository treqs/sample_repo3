# [acceptancetest id=AT0002 story=US0006]
Feature: Send invitation by email

    Scenario: Send invitation email
        Given a user creates a meeting event
        When another user selected as participant of the event
        Then the participant receives the event by email